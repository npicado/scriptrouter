<?php

function callProxyScript()
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'http://52.9.187.253:8080/d841c50/');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl, CURLOPT_TIMEOUT, 2);

    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
        'apikey' => 'Tgi/s0TcTqUXxJy/Jiu9xjyaqozf0TlE/PXOzf0r2SE='
    )));

    if (!($result = curl_exec($curl))) {
        return false;
    }
    var_export($result);

    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    if ($httpCode != 200) {
        return false;
    }

    $result = json_decode($result, true);

    if (!isset($result['status']) || $result['status'] != 'OK') {
        return false;
    }

    return true;
}
