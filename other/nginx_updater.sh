#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

LOGFILE=/home/scriptrouter/logs/general.log
FECHA=`date '+%Y-%m-%d %H:%M:%S'`

if [ -f /home/scriptrouter/www/d841c50/target_map.conf ]; then
  cp /home/scriptrouter/www/d841c50/target_map.conf /etc/nginx/conf.d/target_map.conf
  rm /home/scriptrouter/www/d841c50/target_map.conf
  echo "${FECHA} [NgxUpdater] Map file was detected and move to NGINX config folder." >> ${LOGFILE}
  nginx -t
  if [ $? -eq 0 ]; then
    service nginx reload
    echo "${FECHA} [NgxUpdater] The NGINX service was reloaded successfully." >> ${LOGFILE}
  else
    echo "${FECHA} [NgxUpdater] There was an error in the format of map file." >> ${LOGFILE}
  fi
fi
