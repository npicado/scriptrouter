<?php

header('Content-Type: text/plain');

function tailShell($filepath, $lines = 1) {
    ob_start();
    passthru('tail -'  . $lines . ' ' . escapeshellarg($filepath));
    return trim(ob_get_clean());
}

$lines = 20;
if (isset($_GET['lines'])) {
    $lines = intval($_GET['lines']);
}

if (isset($_GET['request'])) {
    $requestFile = '/home/scriptrouter/logs/request_' . $_GET['request'] . '.log';
    if (file_exists($requestFile)) {
        echo file_get_contents($requestFile);
    } else {
        echo 'The request ' . $_GET['request'] . ' doesn\'t exist.';
    }
} else {
    echo tailShell('/home/scriptrouter/logs/general.log', $lines);
}
