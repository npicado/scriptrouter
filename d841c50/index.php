<?php

function sendResponse($message, $status = 'error', $http_code = 200, $valor = null)
{
    // Set the content type of response
    header('Content-Type: application/json');

    // Prepare the response
    $id = sha1(uniqid('', true));
    $response = array(
        'id' => $id,
        'status' => $status,
        'message' => $message,
    );

    $log = fopen('/home/scriptrouter/logs/general.log', 'a');
    fwrite($log, date('Y-m-d H:i:s') . " [WebService] Request {$id} received - Response status: {$status}.\n");
    fclose($log);

    if (!empty($valor)) {
        file_put_contents("/home/scriptrouter/logs/request_{$id}.log", var_export($valor, true));
    }

    http_response_code($http_code);
    echo json_encode($response, JSON_PRETTY_PRINT) . "\n";
    exit;
}


// Validate request

if (isset($_SERVER['REQUEST_METHOD'])) { // <- just for test environment
    if ($_SERVER['REQUEST_METHOD'] != 'POST') {
        sendResponse('Method not allowed.', 'error', 405);
    }

    if (empty($_POST['apikey']) || $_POST['apikey'] != 'Tgi/s0TcTqUXxJy/Jiu9xjyaqozf0TlE/PXOzf0r2SE=') {
        sendResponse('Authentication is wrong.', 'error', 401);
    }
}


// Prepare the common variables

$query_sites = <<<'EOT'
SELECT DISTINCT
  x.sitedomainname AS domain_name,
  s.externalip AS server_ip,
  x.site_status AS status
FROM
  epm_xref_site_environment AS x
  INNER JOIN epm_server AS s ON x.serverid = s.serverid
EOT;

$query_subsites = <<<'EOT'
SELECT DISTINCT
  x.subsite_domain_name AS domain_name,
  s.externalip AS server_ip,
  x.subsite_status AS status
FROM
  epm_xref_subsite_environment AS x
  INNER JOIN epm_server AS s ON x.serverid = s.serverid
EOT;

$query_dockersites = <<<'EOT'
SELECT
  x.sitedomainname AS domain_name,
  s.externalip AS server_ip,
  d.server_port AS server_port,
  x.site_status AS status
FROM
  epm_xref_site_environment AS x
  INNER JOIN epm_pipeline AS p ON p.xrefid = x.xrefid
  INNER JOIN epm_docker AS d ON d.pipelineid = p.pipelineid
  INNER JOIN epm_server AS s ON s.serverid = d.serverid
EOT;

$query_customdomains = <<<'EOT'
SELECT
  d.domain_name,
  s.externalip AS server_ip,
  x.site_status AS status
FROM
  epm_xref_site_environment AS x
  INNER JOIN epm_domains AS d ON d.xrefid = x.xrefid
  INNER JOIN epm_server AS s ON x.serverid = s.serverid
EOT;

$sites = array();


// Connect to databases

try {
    $db = new PDO("pgsql:dbname=evolpaasmgr;host=db.stratosys.tech", "dev_nestor", "lbit2016");
    $db2 = new PDO("pgsql:dbname=stratopaas;host=db.stratosys.tech", "dev_nestor", "lbit2016");
} catch(PDOException $e) {
    sendResponse($e->getMessage(), 'error');
}


// Get the sites ---------------

if (!$result = $db->query($query_sites)) {
    sendResponse('There was a database error getting the sites.', 'error', 500);
}

while ($reg = $result->fetch(\PDO::FETCH_ASSOC)) {
    $reg['domain_name'] = trim(strtolower($reg['domain_name']));
    $reg['server_ip'] = trim(strtolower($reg['server_ip']));
    $reg['status'] = trim(strtolower($reg['status']));

    if (!empty($reg['domain_name']) && !empty($reg['status']) && $reg['status'] == 'completed') {
        $sites[$reg['domain_name']] = $reg['server_ip'];
    }
}

$result = null;
$reg = null;


// Get the subsites ---------------

if (!$result = $db->query($query_subsites)) {
    sendResponse('There was a database error getting the subsites.', 'error', 500);
}

while ($reg = $result->fetch(\PDO::FETCH_ASSOC)) {
    $reg['domain_name'] = trim(strtolower($reg['domain_name']));
    $reg['server_ip'] = trim(strtolower($reg['server_ip']));
    $reg['status'] = trim(strtolower($reg['status']));

    if (!empty($reg['domain_name']) && !empty($reg['status']) && $reg['status'] == 'completed') {
        $sites[$reg['domain_name']] = $reg['server_ip'];
    }
}

$result = null;
$reg = null;


// Get the docker sites ---------------

if (!$result = $db2->query($query_dockersites)) {
    sendResponse('There was a database error getting the docker sites.', 'error', 500);
}

while ($reg = $result->fetch(\PDO::FETCH_ASSOC)) {
    $reg['status'] = trim(strtolower($reg['status']));
    $reg['domain_name'] = trim(strtolower($reg['domain_name']));
    $reg['server_ip'] = trim(strtolower($reg['server_ip']));
    $reg['server_port'] = trim($reg['server_port']);

    if (!empty($reg['domain_name']) && !empty($reg['status']) && $reg['status'] == 'completed') {
        $sites[$reg['domain_name']] = $reg['server_ip'] . ':' . $reg['server_port'];
    }
}

$result = null;


// Get the custom domains sites ---------------

if (!$result = $db->query($query_customdomains)) {
    sendResponse('There was a database error getting the custom domains sites.', 'error', 500);
}

while ($reg = $result->fetch(\PDO::FETCH_ASSOC)) {
    $reg['status'] = trim(strtolower($reg['status']));
    $reg['server_ip'] = trim(strtolower($reg['server_ip']));
    $reg['domain_name'] = trim(strtolower($reg['domain_name']));
    $reg['domain_name'] = str_replace('http://', '', $reg['domain_name']);
    $reg['domain_name'] = str_replace('https://', '', $reg['domain_name']);

    if (!empty($reg['domain_name']) && !empty($reg['status']) && $reg['status'] == 'completed') {
        $sites[$reg['domain_name']] = $reg['server_ip'];
    }
}

$result = null;
$reg = null;
$db = null;
$db2 = null;
unset($result, $db, $db2, $reg);


// Prepare the target format

$target = "map \$host \$target {\n";
$target .= "  hostnames;\n";
$target .= "  default  \"127.0.0.1:8080\";\n\n";

foreach ($sites as $domain_name => $origin_server) {
    $target .= "  \"{$domain_name}\"  \"{$origin_server}\";\n";
}

$target .= "}\n";


// Save the previous format in target file

file_put_contents('target_map.conf', $target);


// Return response to web server

sendResponse('The map file was synchronized successfully.', 'synchronized', 200, $sites);
